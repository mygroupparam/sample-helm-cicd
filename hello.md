stages:
  - build
  - deploy

variables:
  IMAGE_NAME: registry.gitlab.com/mygroupparam/sample-helm-cicd
  #  DOCKER_HOST: tcp://localhost:2375/

build:
  stage: build
  image: docker:24.0.6
  services:
    - docker:24.0.6-dind
  variables:
    DOCKER_TLS_CERTDIR: /certs
  
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -f Dockerfile --tag ${IMAGE_NAME}:${CI_COMMIT_SHORT_SHA} .
    - docker push ${IMAGE_NAME}:${CI_COMMIT_SHORT_SHA}

deploy:
  stage: deploy
  environment:
    name: dev

  #image: alpine/helm:3.5.4
  image: docker:19.03.5
  script:
    - apk add --no-cache git curl bash
    
    - curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    -  chmod 700 get_helm.sh
    
    - helm upgrade sample-helm-cicd ./deploy --install --set-string image.tag=${CI_COMMIT_SHORT_SHA} --namespace sample-helm-cicd --timeout 30m0s

